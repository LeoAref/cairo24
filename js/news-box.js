$(document).ready(function(){

    var windowH = $(window).height();
    var windowW = $(window).width();
    $(window).resize(function(){ windowH = $(window).height(); windowW = $(window).width(); });

    /*var boxW = $('#news-box').width();
    var boxH = $('#news-box').height();
    $('.news-content').outerWidth(boxW - 360).outerHeight(boxH - 40); // 360 = adv(300) + 20(side margin) + 40(padding 20*2)
    $('.news-sidebar').outerHeight(boxH - 40);*/

    $('.news-box-icon').colorbox({width: '100%', height: '100%',fixed:true});

    var url = window.location.href;

    $(document).bind('cbox_complete',function(){
        var boxW = $('.pop-up-box').width();
        var boxH = $('.pop-up-box').height();
        $('.news-content').outerWidth(boxW - 360).outerHeight(boxH - 40); // 360 = adv(300) + 20(side margin) + 40(padding 20*2)
        $('.news-sidebar').outerHeight(boxH - 40);
        $('html').css({overflow:'hidden',height:'100%'});
        $('.news-content,.news-sidebar').niceScroll({
            autohidemode:false,
            railalign:'left',
            cursorwidth:'8px',
            cursorborder:0,
            cursorborderradius:'5px',
            cursorcolor:'#aeaeae',
            background:'#f9f9f9'
        });
        $('.news-content').corner('5px tr br');
        window.history.pushState('','', 'news-box.html');


        $('#album-box .album-images').height($('#album-box .album-images img:first-child').height());
        $('#album-box .album-images img').css({position:'absolute',display:'block'});
        $('#album-box .img-description').text($('#album-box .album-images img:first-child').attr('img-caption'));
        $('#album-box .album-images img:first-child').addClass('current');

        if($('.pop-up-box').attr('id') == 'album-box'){

        var myElement = document.getElementById('album-cont');
        var mc2 = new Hammer(myElement);
        mc2.on("swipeleft swiperight", function(ev) {
            if(ev.type == 'swipeleft'){boxAlbum_prv()}
            if(ev.type == 'swiperight'){boxAlbum_nxt()}
        });
    }
    });

    $(document).bind('cbox_cleanup', function() {
        $('.nicescroll-rails').remove();
        $('html').css({overflow:'visible',height:'auto'});
        window.history.pushState('','', url);
    });


    $(document).on('click','#album-box .arrow-left',function(){boxAlbum_nxt();});
    $(document).on('click','#album-box .arrow-right',function(){boxAlbum_prv();});
    $(document).on('dragstart','#album-cont img', function(event) {event.preventDefault();});
    $(document).keydown(function(e){
		if (e.keyCode == 39){boxAlbum_prv();} // right pressed //
		if (e.keyCode == 37){boxAlbum_nxt();} // left pressed //
	});

    var boxAlbum_nxt = function(){
        var crt = $('#album-box .album-images img.current');
        var nxt = crt.next();
        if(nxt.length == 0) nxt = $('#album-box .album-images img:first-child');
        if(crt.is(':animated') || nxt.is(':animated')) return;
        var imgW = $('#album-box .album-images img:first-child').width();
        crt.delay(10).animate({right:-imgW},function(){crt.removeClass('current');});
        nxt.css('right',imgW).addClass('current').animate({right:0});
        $('#album-box .img-description').text(nxt.attr('img-caption'));
    };

    var boxAlbum_prv = function(){
        var crt = $('#album-box .album-images img.current');
        var prv = crt.prev();
        if(prv.length == 0) prv = $('#album-box .album-images img:last-child');
        if(crt.is(':animated') || prv.is(':animated')) return;
        var imgW = $('#album-box .album-images img:first-child').width();
        crt.delay(10).animate({right:imgW},function(){crt.removeClass('current');});
        prv.css('right',-imgW).addClass('current').animate({right:0});
        $('#album-box .img-description').text(prv.attr('img-caption'));
    };




});