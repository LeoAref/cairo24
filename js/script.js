$(document).ready(function(){

    var scrollTop = $('.header .header-top').outerHeight();
    $('.header .site-menu').outerWidth($('.header .site-menu').outerWidth());
    $(window).scroll(function(){
        if($(window).scrollTop() >= scrollTop){
            $('.header .site-menu').addClass('fixed').css('right',($(window).width()-$('.header .site-menu').outerWidth())/2);
        }
        if($(window).scrollTop() < scrollTop){
            $('.header .site-menu').removeClass('fixed').css('right',0);
        }
    });

    // ----------- corner ----------- //
    $('.header .user-profile-image a,.toggle-video-content,.section-news .small-div,.section-news .large-div,.header .user-profile-menu,.header .home-icon,.home-section .sec-items > div,.home-section.system1 .large-div,.home-section.system1 .small-div,#services .service-cont,.most-read-small > div, #most-comment .large-div, #most-read .large-div,.single-news-tags li a').corner('4px');
    $('.home-section .video-large, .home-section .video-small').corner('14px');
    $('.header .site-menu').corner('8px bl br');
    $('.header .header-top').corner('8px tl tr');
    $('.main .sec-white-bg,.album-div .news-icons').corner('4px bl br');
    $('.album-large-img,.single-related-videos .video-small .video-img,.feed-mogaz div').corner('4px tl tr');
    $('.slider .slider-main .news-icons').corner('4px br');
    $('.slider .slider-main').corner('4px tr br');
    $('.slider .slider-main .slider-main-img').corner('4px tr');
    $('.slider .slider-thums .slider-thum:first-child').corner('5px tl');
    $('.slider .slider-thums .slider-thum:last-child').corner('5px bl');
    $('.section-title').corner('6px tl tr');
    //if($('.header .user-profile-image img').width() < 45) { $('.header .user-profile-image').corner('18px'); } else {$('.header .user-profile-image').corner('26px');}
    $('.header .search,#single .home-section.system1 .large-div').corner('8px');
    $('.single-album-thum,.single-related-albums .sec-white-bg,.widget-bullets li span').corner('5px');

    // ---------- folders-sec --------- //
    $('#folders-sec .sec-items > div:first-child img').load(function(){ $('#folders-sec .sec-items > div a.title').height(Math.floor(($('#folders-sec .sec-items > div:first-child img').height()-14)/16)*16+'px'); $('.home-section .sec-items > div a').dotdotdot({watch: "window"}); });
    
	// --------- mobile menu --------- //
    //var mobileMenu_H = ($('.header .large-menu li').length-1)*$('.header .large-menu li').outerHeight();
    var mobileMenu_H = 260;
    $('.dropdown-title').click(function(){
        if($('.header .large-menu').is(':animated')) return;
        if($('.header .large-menu').height() == 0){
            $('.header .large-menu').animate({height:mobileMenu_H},function(){$('.header .large-menu').css('overflow-y','scroll')});
        }else{
            $('.header .large-menu').css('overflow','hidden').animate({height:0});
        }
    });

    // ---------- dot dot dot ---------- //
    $('.home-section.system1 .large-div a.title,.related-news-item a,.section-news .news-description,.home-section .sec-items > div a, .video-small a.title, .home-section .video-large a.title,.most-most.home-section .large-div a.title, #home-albums a.title, .slider .slider-thums .thum-title,.slider .main-title,.feed-elem a,.home-section .sec-items > div a,.home-section.system1 .small-div a.title,#most-read.home-section .small-div a.title, #most-comment.home-section .small-div a.title,#most-read.home-section .large-div a.title, #most-comment.home-section .large-div a.title').dotdotdot({watch: "window"});

    var service_W = [];
    $('#services .service-cont').each(function(){ service_W.push($(this).height()); });
    $('#services .service-cont').height(Math.max.apply(Math, service_W));


    // ------------ slider ------------- //
    $('.slider .slider-thums .thum-title').click(function(e){e.preventDefault();});
    $('.slider .slider-thums .slider-thum').click(function(){
        if(slider_timer){clearTimeout(slider_timer); slider_timer = null;}
        slider_timer = setTimeout(function(){sliderRotator()},5000);

        $('.slider .slider-thums .slider-thum').removeClass('active');
        var that = $(this).addClass('active');
        var title = that.find('.thum-title');
        var img = that.find('.slider-large-img img');
        var like = that.find('.like a');
        var share = that.find('.share a');
        var comment = that.find('.comment a');

        var slider_main = $('.slider-main');
        slider_main.find('.slider-main-img img').attr('src',img.attr('src'));
        slider_main.find('.main-title').attr('href',title.attr('href')).text(title.text());
        slider_main.find('.like a').attr('href',like.attr('href'));
        slider_main.find('.share a').attr('href',share.attr('href'));
        slider_main.find('.comment a').attr('href',comment.attr('href'));
    });

    var slider_timer;
    slider_timer = setTimeout(function(){sliderRotator()},5000);
    function sliderRotator(){
        var crt = $('.slider .slider-thums .slider-thum.active');
        var nxt = crt.next();
        if(nxt.length == 0) nxt = $('.slider .slider-thums .slider-thum:first-child');

        crt.removeClass('active');
        nxt.addClass('active');

        var title = nxt.find('.thum-title');
        var img = nxt.find('.slider-large-img img');
        var like = nxt.find('.like a');
        var share = nxt.find('.share a');
        var comment = nxt.find('.comment a');

        var slider_main = $('.slider-main');
        slider_main.find('.slider-main-img img').attr('src',img.attr('src'));
        slider_main.find('.main-title').attr('href',title.attr('href')).text(title.text());
        slider_main.find('.like a').attr('href',like.attr('href'));
        slider_main.find('.share a').attr('href',share.attr('href'));
        slider_main.find('.comment a').attr('href',comment.attr('href'));

        if(slider_timer){clearTimeout(slider_timer); slider_timer = null;}
        slider_timer = setTimeout(function(){sliderRotator()},5000);
    }

    // ------- top stories ----------- //
    var top_W,window_W,li_W;
    topStories();
    function topStories() {
        window_W = $(window).width();
        top_W = $('.top-story').outerWidth() -16;
        if (window_W < 640) {
            li_W = Math.floor(top_W / 2);
            top_W = li_W * 2;
            $('.top-story .top-stories-cont ul').width($('.top-story .top-stories-cont ul li').length * li_W);
            $('.top-story .top-stories-cont li').outerWidth(li_W);
            $('.top-story .top-stories-cont').outerWidth(2 * li_W);
        } else if (window_W > 640) {
            li_W = Math.floor(top_W / 3);
            top_W = li_W * 3;
            $('.top-story .top-stories-cont li').outerWidth(li_W);
            $('.top-story .top-stories-cont ul').width($('.top-story .top-stories-cont ul li').length * li_W);
            $('.top-story .top-stories-cont').outerWidth(3 * li_W);
        }
    }

    var topStory_timer, topStory_period = 8000;
    topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
    function topStoryRotator() {
        var that = $('.top-story .top-stories-cont .arrow-left');
        if(topStories_ul.is(':animated')) return;
        if(topStory_timer){clearTimeout(topStory_timer); topStory_timer = null;}
        topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
        if(that.hasClass('disabled')){
            topStories_ul.animate({right: ul_right= 0},1000,function(){
                that.removeClass('disabled').addClass('active');
                that.siblings('.arrow-right').removeClass('active').addClass('disabled');
            });
        }else{
            that.siblings('.arrow-right').removeClass('disabled').addClass('active');
            var max_right = top_W - topStories_ul.width();
            var ul_right = parseInt(topStories_ul.css('right'));
            if(ul_right-li_W >= max_right){
                topStories_ul.animate({right: ul_right-= li_W},function(){
                    if(ul_right-li_W < max_right){ that.removeClass('active').addClass('disabled'); }
                });
            }
        }
    }

    var topStories_ul = $('.top-story .top-stories-cont ul');
    $(document).on('click','.top-story .top-stories-cont .arrow-left',function(){
        var that = $(this);
        if(that.hasClass('disabled') || topStories_ul.is(':animated')) return;
        if(topStory_timer){clearTimeout(topStory_timer); topStory_timer = null;}
        topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
        that.siblings('.arrow-right').removeClass('disabled').addClass('active');
        var max_right = top_W - topStories_ul.width();
        var ul_right = parseInt(topStories_ul.css('right'));
        if(ul_right-li_W >= max_right){
            topStories_ul.animate({right: ul_right-= li_W},function(){
                if(ul_right-li_W < max_right){ that.removeClass('active').addClass('disabled'); }
            });
        }
    });
    $(document).on('click','.top-story .top-stories-cont .arrow-right',function(){
        var that = $(this);
        if(that.hasClass('disabled') || topStories_ul.is(':animated')) return;
        if(topStory_timer){clearTimeout(topStory_timer); topStory_timer = null;}
        topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
        that.siblings('.arrow-left').removeClass('disabled').addClass('active');
        var max_right = 0;
        var ul_right = parseInt(topStories_ul.css('right'));
        if(ul_right != max_right){
            topStories_ul.animate({right: ul_right+= li_W},function(){
                if(ul_right == max_right){ that.removeClass('active').addClass('disabled'); }
            });
        }
    });

    $('#top-story img').on('dragstart', function(event) {
        event.preventDefault();
    });

    if($('body').attr('id') == 'index'){
        var myElement = document.getElementById('top-story');
        var mc = new Hammer(myElement);

        mc.on("panleft panright", function(ev) {
            if(ev.type == 'panleft'){
                if(topStory_timer){clearTimeout(topStory_timer); topStory_timer = null;}
                var that = $('.top-story .top-stories-cont .arrow-right');
                topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
                if(that.hasClass('disabled') || topStories_ul.is(':animated')) return;
                that.siblings('.arrow-left').removeClass('disabled').addClass('active');
                var max_right = 0;
                var ul_right = parseInt(topStories_ul.css('right'));
                if(ul_right != max_right){
                    topStories_ul.animate({right: ul_right+= li_W},function(){
                        if(ul_right == max_right){ that.removeClass('active').addClass('disabled'); }
                    });
                }
            }
            if(ev.type == 'panright'){
                var that = $('.top-story .top-stories-cont .arrow-left');
                if(that.hasClass('disabled') || topStories_ul.is(':animated')) return;
                if(topStory_timer){clearTimeout(topStory_timer); topStory_timer = null;}
                topStory_timer = setTimeout(function(){topStoryRotator()},topStory_period);
                that.siblings('.arrow-right').removeClass('disabled').addClass('active');
                var max_right = top_W - topStories_ul.width();
                var ul_right = parseInt(topStories_ul.css('right'));
                if(ul_right-li_W >= max_right){
                    topStories_ul.animate({right: ul_right-= li_W},function(){
                        if(ul_right-li_W < max_right){ that.removeClass('active').addClass('disabled'); }
                    });
                }
            }
        });
    }


    // --------- window resize --------- //
    $(window).resize(function(){
        topStories();
        mobileMenu_H = ($('.header .large-menu li').length-1)*$('.header .large-menu li').outerHeight();
        service_W = [];
        $('#services .service-cont').each(function(){ service_W.push($(this).height()); });
        $('#services .service-cont').height(Math.max.apply(Math, service_W));
        $('#folders-sec a.title').css('height',Math.floor(($('#folders-sec .sec-items > div:first-child img').height()-14)/16)*16);
        $('.header .site-menu').outerWidth($('.header .header-top').outerWidth()).css('right',($(window).width()-$('.header .header-top').outerWidth())/2);
    });

    $('.news-img').mousemove(function(event) {
        var imgOffset = $(this).offset();
        var imgCenterX = $(this).width()/2;
        var imgCenterY = $(this).height()/2;
        var xPos = event.pageX;
        var yPos = event.pageY;
        var relX = event.pageX - imgOffset.left;
        var relY = event.pageY - imgOffset.top;
        var Xmove = (relX - imgCenterX) * 0.05;
        var Ymove = (relY - imgCenterY) * 0.05;
        //$(this).find('img').css({right:Xmove,top:Ymove});
    });

    $('.news-img').hover(function(){},function(){$(this).find('img').css({top:0,right:0})});

    // video content collapse/expand
    $('.toggle-video-content').click(function(){
        $('.video-content').toggleClass('auto');
    });

    // ----------- live-widget ----------- //
    $('.widget-divs li:first-child .news-img img').load(function(){
        $('#live-widget .widget-divs').outerHeight($('.widget-divs li:first-child').addClass('current').outerHeight());
        $('.widget-divs li img').height($('.widget-divs li:first-child img').height());
        $('.widget-divs li').css({position:'absolute',display:'block'}).width($('.widget-divs li:first-child img').width());
    });

    var widgetWidth = $('#live-widget .widget-divs').width();

    var live_widgets_nxt = function(){
        var crt = $('.widget-divs li.current');
        var nxt = crt.next();
        if(nxt.length == 0) nxt = $('.widget-divs li:first-child');
        nxt.css({right:widgetWidth,'z-index':2}).animate({right:0},function(){nxt.addClass('current').css({'z-index':1}); crt.removeClass('current').css({'z-index':0});});
        var index = $('.widget-divs li').index(nxt) + 1;
        $('.widget-bullets li').removeClass('active');
        $('.widget-bullets li:nth-child('+index+')').addClass('active');
    }

    var live_widgets_prv = function(){
        var crt = $('.widget-divs li.current');
        var prv = crt.prev();
        if(prv.length == 0) prv = $('.widget-divs li:last-child');
        prv.css({right:-widgetWidth,'z-index':2}).animate({right:0},function(){prv.addClass('current').css({'z-index':1}); crt.removeClass('current').css({'z-index':0});});
        var index = $('.widget-divs li').index(prv) + 1;
        $('.widget-bullets li').removeClass('active');
        $('.widget-bullets li:nth-child('+index+')').addClass('active');
    }

    $('.widget-bullets li').click(function(){
        var bulletsIndex = $('.widget-bullets li').index($(this)) + 1;
        var divsIndex = $('.widget-divs li').index($('.widget-divs li.current')) + 1;
        if(bulletsIndex == divsIndex) return;
        $('.widget-bullets li').removeClass('active');
        $('.widget-bullets li:nth-child('+bulletsIndex+')').addClass('active');
        var crt = $('.widget-divs li.current');
        var nxt = $('.widget-divs li:nth-child('+bulletsIndex+')');
        if(divsIndex > bulletsIndex){
            nxt.css({right:-widgetWidth,'z-index':2}).animate({right:0},function(){nxt.addClass('current').css({'z-index':1}); crt.removeClass('current').css({'z-index':0});});
        }else{
            nxt.css({right:widgetWidth,'z-index':2}).animate({right:0},function(){nxt.addClass('current').css({'z-index':1}); crt.removeClass('current').css({'z-index':0});});
        }
    });

    $('#live-widget .arrow-left').click(function(){ live_widgets_nxt() });
    $('#live-widget .arrow-right').click(function(){ live_widgets_prv() });
    $('#live-widget img').on('dragstart', function(event) {
        event.preventDefault();
    });

    if($('.pop-up-box').attr('id') == 'index'){
        var myElement = document.getElementById('live-widget');
        var mc3 = new Hammer(myElement);

        mc3.on("swipeleft swiperight", function(ev) {
            if(ev.type == 'swipeleft'){live_widgets_prv()}
            if(ev.type == 'swiperight'){live_widgets_nxt()}
        });
    }

    // fixed-sidebar
    $(".fixed-sidebar").sticky({topSpacing:60,getWidthFrom:$('.sidebar'),responsiveWidth:true});

});

    var fixedsidePosition;
$(window).load(function(){
	$('#folders-sec .sec-items > div a.title').height(Math.floor(($('#folders-sec .sec-items > div:first-child img').height()-14)/16)*16+'px').fadeIn();
});